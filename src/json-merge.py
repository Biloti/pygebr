#!/usr/bin/python3

import sys
import json as js
import re

# Strip all path from url, keeping the string after the last "/"
def basename(url):
    tokens = re.split('/', url)
    return tokens[-1]

# Load a json from disk
# TODO: look for json in URL too
def load(json):
    return js.load(open(basename(json)))

# Resolve every $ref in json
def full_json(json):

    if type(json) == dict:
        keys = json.keys()
        for key in keys:
            if '$ref' == key:
                json = full_json (load(json[key]))
            else:
                json[key] = full_json (json[key])
    elif isinstance(json, list):
        k = 0;
        for item in json:
            if isinstance(item, dict):
                json[k] = full_json (item)
            k = k + 1

    return json

if (len(sys.argv) < 2):
    print("Usage %s json"%sys.argv[0])
    exit(-1)

print(js.dumps(full_json(load(sys.argv[1]))))
