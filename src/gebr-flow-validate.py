#!/usr/bin/python3
import sys
import pygebr as pgbr

if len(sys.argv) < 2:
    print("Usage: %s flow1.json flow2.json ...")
    sys.exit(-1)


for fn in sys.argv:

    if fn[-5:] != ".json":
        continue

    flow = pgbr.Flow(filename=fn)
    valid, errors = flow.validate(True)

    if valid:
        print("%s validates correctly against schema"%fn)
    else:
        print("%s doesn't validate correctly against schema"%fn)

        for error in errors:
            print(error)
        print("")
