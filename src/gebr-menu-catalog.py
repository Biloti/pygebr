#!/usr/bin/python3
import os, re
from pygebr import Setup, Flow
import json as JSON

def menusort(menu):
    return menu['title']

def scan_dir(base, catalog):
    for root, dirs, files in os.walk(base):
        for file in files:
            if re.search(".json$",file):
                fname = os.path.join(root, file)
                flow = Flow(filename = fname)
                tags = flow.tags()
                for tag in tags:
                    tagl = tag.lower()
                    if tagl in catalog:
                        catalog[tagl].append({'title': flow.title(),
                                              'description': flow.description(),
                                              'menu': file[:-5],
                                              'file': fname})
                    else:
                        catalog[tagl] = [{'title': flow.title(),
                                          'description': flow.description(),
                                          'menu': file[:-5],
                                          'file': fname}]

        for dir in dirs:
            scan_dir(dir, catalog)

setup = Setup()

catalog = {}
for path in setup.menudirs():
    scan_dir(path, catalog)

sorted = {key:catalog[key] for key in sorted(catalog.keys())}
for tag in sorted.keys():
    sorted[tag].sort(key=menusort)

print(JSON.dumps(sorted))
