#!/usr/bin/python3
import sys
from optparse import OptionParser
from pygebr import Flow

parser = OptionParser()
parser.add_option("-v", "--verbose", action="store_true", dest="verbose",
                  default=False, help="print more detailed ouput")
parser.add_option("-s", "--setonly", action="store_true", dest="setonly",
                  default=False, help="print only parameters that are set")

parser.add_option("-f", "--flow", action="store", dest="file",
                  type="string", help="flow's file name")
(options, args) = parser.parse_args()


if options.file == None:
    print("Error: flow file not informed.\nTry: %s -h"%sys.argv[0])
    exit(-1)

flow = Flow(filename=options.file)
flow.dump(verbose=options.verbose,setonly=options.setonly)
