#!/usr/bin/python3
import sys
import json
from pygebr import Flow

n = len(sys.argv)

if (n < 3):
    print("Usage: %s flow1.json flow2.json ... flowN.json > merge.json"%sys.argv[0])
    exit(-1)

base = Flow(filename=sys.argv[1])

for k in range(2,n):
    flow = Flow(filename=sys.argv[k])
    base.merge(flow)

print(json.dumps(base.json()))
