#!/usr/bin/python3
import sys
import pygebr as pgbr

if (len(sys.argv) < 2):
    print("Usage %s flow.json"%sys.argv[0])
    exit(-1)

flow = pgbr.Flow(filename=sys.argv[1])
try:
    cmd = flow.eval()
except Exception as err:
    print("Unable to produce a command line for %s"%sys.argv[1])
    print("Error: " + str(err))
    sys.exit(-1)

print(cmd)
