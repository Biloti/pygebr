# PyGêBR

Assemble, inspect, edit and run processing flows with python scripts.

Was it not clear enough? Well, take a look at our [wiki](https://gitlab.com/Biloti/pygebr/-/wikis/home).
