{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "described-certification",
   "metadata": {},
   "source": [
    "# Building your own menus for PyGêBR"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "sunset-christopher",
   "metadata": {},
   "source": [
    "PyGêBR comes with many _menus_ for the common seismic-processing packages Madagascar and Seismic Un*x, as well as for some stand-alone tools. But it is not limited to those. You can integrate new programs to PyGêBR, even your own programs, by building _menus_ to them."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3bc0cf45-5dd5-4c94-91e7-8d48c4dd138f",
   "metadata": {},
   "source": [
    "In this notebook, we show how to construct a menu to describe a command-line program, in such a way it can be used from PyGêBR."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "occupational-diary",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "signed-shooting",
   "metadata": {},
   "source": [
    "**PyGêBR** represents command-line programs and their chaining to compose processing flows. For example, consider the command line below, in which the program `sfscale` is run.\n",
    "\n",
    "`sfscale <in.rsf >out.rsf axis=0 pclip=100. dscale=1.5`\n",
    "\n",
    "In this line, `sfscale` is the _executable_.\n",
    "\n",
    "\n",
    "`<in.rsf` informs that the file `in.rsf` is being feed in through the standart input, from which the program reads data. `>out.rsf` informs that data written by the program in the standard output will be saved in file `out.rsf`.\n",
    "\n",
    "The rest of the elements in the command linea are _parameters_ which tune the program's behavior.\n",
    "\n",
    "`axis` is a parameter that asks for an integer value (`0` in the line above). The parameters `pclip` and `dscale` both ask for real values. The text used to define these parameters (`axis=`, `pclip=` e `dscale=`) are their _keywords_. To PyGêBR, a keyword has all characters required for parameter definition in the command line. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "neural-digit",
   "metadata": {},
   "source": [
    "## Parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "durable-building",
   "metadata": {},
   "source": [
    "Beyond numerical valued parameters (real or integers), PyGêBR supports other types of parameters. Below you see the full list of supported parameters types.\n",
    "\n",
    " - **integer**: A parameter that expects an integer value as argument.  \n",
    "   Ex.: `ns=10`\n",
    " - **range**: A parameter that expects a numerical value as argument within a finite interval (with minimum and maximum).  \n",
    "   Ex.: `month=10`\n",
    " - **integers**: A parameter that expects a list of integer values as argument.  \n",
    "   Ex.: `np=5,4,8,10`\n",
    " - **float**: A parameter that expects a real value as argument.  \n",
    "   Ex.: `dx=2.5`\n",
    " - **floats**: A parameter that expects a list of real values as argument.  \n",
    "   Ex.: `vel=1.5,2.2,4.5` \n",
    " - **string**: A parameter that expects a text as argument.  \n",
    "   Ex.: `label=\"Distância (km)\"`\n",
    " - **strings**: A parameter that expects a list of texts as argument.  \n",
    "   Ex.: `prefixes=\"Anti,Pos\"`\n",
    " - **flag**: A boolean parameter that may or may not be in the command-line.  \n",
    "   Ex.: `--verbose`\n",
    " - **file**: A parameter that expects a file name as argument.  \n",
    "   Ex.: `--export=image.png`\n",
    " - **path**: A parameter that expects a path in the filesystem as argument.  \n",
    "   Ex.: `--tmpdir=/tmp`\n",
    " - **enum**: A parameter that expect as argument a value from within a list of predefined possible values.  \n",
    "   Ex.: `interpolation=linear` (where the options would be `linear`, `quadratic` or `cubic`)\n",
    " - **section**: Not really a program parameter, but rather a _meta parameter_ used to better organize parameters in the interface. After adding a section parameter, all subsequent parameters will be grouped together within the section. A section parameter has only a `title`.\n",
    " - **comment**: Another _meta parameter_. It is used for adding a text block between parameters in the widget. In this way it can be used to insert comments that clarify the meaning or usage of a particular parameter, for example. A comment parameter has only a `description`.\n",
    "  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "comparable-basic",
   "metadata": {},
   "source": [
    "## Program"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "measured-intelligence",
   "metadata": {},
   "source": [
    "PyGêBR defines a class to represent programs. To specify a program, some information must be provided. They are:\n",
    "\n",
    " - **executable**: the name of the executable command for the program (`sfscale`, in the example above).\n",
    " - **title**: short title used to present the program to users.\n",
    " - **description**: one-line sentence to state programs purpose.\n",
    " - **url**: a URL where more information about the program can be found.\n",
    " - **authors**: list of program's authors.\n",
    " - **stdin**: flag indicating whether the program reads data from standard in.\n",
    " - **stdout**: flag indicating whether the program writes data to standard output.\n",
    " - **stderr**: flag indicating whether the program writes data to standard error.\n",
    "\n",
    "With the program created, it remains to add parameters to it."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "amended-pollution",
   "metadata": {},
   "source": [
    "## Menu"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "specialized-logging",
   "metadata": {},
   "source": [
    "_Menus_ are used to gather programs and make them available to build processing flows. Most menus contain only one program, but sometimes it may be useful to build a menu with two or more programs, which are usually employed in that particular way. To create a menu, you have to inform:\n",
    "\n",
    " - **title**: Short text to refer the menu.\n",
    " - **description**: one-line sentence describing the menu's purpose.\n",
    " - **authors**: list of the menu's authors.\n",
    " - **tags**: list of keywords used to classify the menu, to allow searching for the menu.\n",
    " \n",
    "With the menu created, it remains to add programs to it."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "focal-democrat",
   "metadata": {},
   "source": [
    "# Building a menu step by step"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "suspected-prefix",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import all modules from PyGêBR\n",
    "\n",
    "from pygebr import *"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "desperate-north",
   "metadata": {},
   "source": [
    "## Credits"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "conceptual-stroke",
   "metadata": {},
   "source": [
    "We always try to make clear who are the authors of each program, menu and flow. You, as the menus creator, should be granted for that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "super-collaboration",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create \"you\" to be used as the menu's author\n",
    "\n",
    "you = Person(name=\"John Doe\",\n",
    "             email=\"john@email.com\",\n",
    "             institution=\"University Somewhere\",\n",
    "             homepage=\"https://www.your.page.com/\")\n",
    "\n",
    "# The credit holder for Madagascar program\n",
    "\n",
    "rsf = Person(name=\"Madagascar\",\n",
    "             institution=\"University of Texas at Austin\",\n",
    "             homepage=\"https://www.reproducibility.org/\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "stuck-handle",
   "metadata": {},
   "source": [
    " ## The program"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "marine-winner",
   "metadata": {},
   "source": [
    "In this example, we will construct a menu to hold only the `sfscale` program."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "reduced-worst",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a representation for program sfscale\n",
    "\n",
    "# To be able to insert this program into processing flows\n",
    "# it is necessary to inform how the program deals with\n",
    "# the system standard input and output. In this particular\n",
    "# case, sfscale reads from standard input and writes to\n",
    "# standard output.\n",
    "\n",
    "title = \"SF Scale\"\n",
    "desc = \"Scale RSF data\"\n",
    "\n",
    "prog = Prog(title=title,           # title for the program\n",
    "            description=desc,      # short description\n",
    "            executable=\"sfscale\",  # executable\n",
    "            url=\"https://reproducibility.org/wiki/Guide_to_madagascar_programs#sfscale\",\n",
    "            authors=rsf,           # program's author\n",
    "            # states that the program reads from the standar input\n",
    "            stdin=True,    # that's the default and may be omitted\n",
    "            # stats that the program writes to the standard output\n",
    "            stdout=True,   # that's the default and may be omitted\n",
    "            # stats that the program writes to the standard error\n",
    "            stderr=True    # that's the default and may be omitted\n",
    "           )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "surprised-suspension",
   "metadata": {},
   "source": [
    "Let us now define each of sfscale's parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "constitutional-prospect",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the integer parameter axis, which is\n",
    "# passed as axis=2 in the command line, for example.\n",
    "\n",
    "par = Param(ptype=\"integer\",   # parameter type\n",
    "            # keyword for the parameter\n",
    "            keyword=\"axis=\",        \n",
    "            # One-line description used to display this parameter in the UI\n",
    "            description=\"Scale by maximum in the dimensions up to this axis\", \n",
    "            # Informs that this parameter may be omited\n",
    "            required=False,\n",
    "            # Informs that this parameter may be defined only once\n",
    "            multiple=False,\n",
    "            # Defines a default value to be used in UI\n",
    "            default=0\n",
    "           )\n",
    "\n",
    "# Add the parameter to the program object.\n",
    "\n",
    "prog.parameter_add(par)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "statutory-practitioner",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a new parameter of float type and add it\n",
    "# directly this time.\n",
    "\n",
    "prog.parameter_add(Param(ptype=\"float\",\n",
    "                         keyword=\"dscale=\",\n",
    "                         description=\"Scale factor\",\n",
    "                         required=False,\n",
    "                         multiple=False,\n",
    "                         default=1.0)\n",
    "                  )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "liquid-jonathan",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a parameter of range type. In this case, it isn't\n",
    "# possible to add it directly, since some parameter's\n",
    "# attributes can't be informed at the moment the parameter\n",
    "# is declared.\n",
    "\n",
    "par = Param(ptype=\"range\",\n",
    "            keyword=\"pclip=\",\n",
    "            description=\"Data clip percitile\",\n",
    "            required=False,\n",
    "            multiple=False,\n",
    "            default=100)\n",
    "\n",
    "# Define the interval where for acceptable values for\n",
    "# this parameter. Also informs the step used to increment\n",
    "# or decrement the parameter's value in the UI, as well as\n",
    "# how many decimal digits should be presented.\n",
    "\n",
    "par.range([0,100], vinc=0.5, vdigits=2)\n",
    "\n",
    "# Add the parameter to the program object.\n",
    "\n",
    "prog.parameter_add(par)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "engaged-karen",
   "metadata": {},
   "source": [
    "All parameters that control `sfscale` behavior have be declared and added to program object, which is now done."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "canadian-groove",
   "metadata": {},
   "source": [
    "## The menu, at last"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "charitable-stephen",
   "metadata": {},
   "outputs": [],
   "source": [
    "# As this menu will have only one program, it is usual employ the same\n",
    "# title and description used for the program. The author however isn't\n",
    "# the same. You can inform a list of tags used to classify the menu,\n",
    "# for indexing purposes.\n",
    "\n",
    "menu = Flow(title=title,\n",
    "            description=desc,\n",
    "            authors=you,\n",
    "            tags=['rsf','madagascar'])\n",
    "\n",
    "# Add the program to the menu object.\n",
    "\n",
    "menu.program_add(prog)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "transsexual-removal",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Show a report describing the menu\n",
    "\n",
    "menu.dump(verbose=True,setonly=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "taken-circle",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Finally, save the menu\n",
    "\n",
    "menu.save(\"/tmp/sfscale.json\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "prerequisite-american",
   "metadata": {},
   "source": [
    "Now the file `sfscale.json` is a menu for program  `sfscale` and could be used by `LoadFlow` command to build processing flows. Note that `LoadFlow` looks for menus in some directories defined in the PyGêBR setup. Therefore, to be able to find your menu, save it into one of those directories."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "confidential-mitchell",
   "metadata": {},
   "outputs": [],
   "source": [
    "# To discover where `LoadFlow` looks for menus, run\n",
    "\n",
    "setup = Setup()\n",
    "print(setup)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
